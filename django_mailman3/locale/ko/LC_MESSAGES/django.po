# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 16:07+0000\n"
"PO-Revision-Date: 2022-10-30 14:07+0000\n"
"Last-Translator: 이정희 <daemul72@gmail.com>\n"
"Language-Team: Korean <https://hosted.weblate.org/projects/gnu-mailman/"
"django-mailman3/ko/>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.14.2-dev\n"

#: forms.py:32
msgid "Username"
msgstr "사용자이름"

#: forms.py:33
msgid "First name"
msgstr "이름"

#: forms.py:34
msgid "Last name"
msgstr "성"

#: forms.py:36
msgid "Time zone"
msgstr "시간대"

#: forms.py:43
msgid "A user with that username already exists."
msgstr "그 사용자명을 싸는 사용자가 이미 존재합니다."

#: templates/account/email.html:6
#: templates/django_mailman3/profile/base.html:17
msgid "Account"
msgstr "계정"

#: templates/account/email.html:11
msgid "The following e-mail addresses are associated with your account:"
msgstr "다음 이메일 주소가 계정과 연결되어 있습니다:"

#: templates/account/email.html:25
msgid "Verified"
msgstr "검증됨"

#: templates/account/email.html:27
msgid "Unverified"
msgstr "검증되지 않음"

#: templates/account/email.html:29
msgid "Primary"
msgstr "최우선"

#: templates/account/email.html:35
msgid "Make Primary"
msgstr "최우선으로 지정"

#: templates/account/email.html:36
msgid "Re-send Verification"
msgstr "인증을 다시 보냅니다"

#: templates/account/email.html:37 templates/socialaccount/connections.html:34
msgid "Remove"
msgstr "제거"

#: templates/account/email.html:44
msgid "Warning:"
msgstr "경고:"

#: templates/account/email.html:44
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, etc."
msgstr ""
"현재 이메일 주소 설정을 하지 않았습니다. 알림을 받거나 비밀번호를 재설정하기 "
"위해서는 정말로 이메일 주소를 추가해야 합니다."

#: templates/account/email.html:49
msgid "Add E-mail Address"
msgstr "이메일 주소를 추가"

#: templates/account/email.html:55
msgid "Add E-mail"
msgstr "이메일 추가"

#: templates/account/email.html:66
msgid "Do you really want to remove the selected e-mail address?"
msgstr "선택된 이메일 주소를 정말로 삭제하시겠습니까?"

#: templates/account/email_confirm.html:6
#: templates/account/email_confirm.html:10
msgid "Confirm E-mail Address"
msgstr "이메일 주소 확인"

#: templates/account/email_confirm.html:16
#, python-format
msgid ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."
msgstr ""
"<a href=\"mailto:%(email)s\">%(email)s</a>가 사용자 %(user_display)s의 이메"
"일 주소인지 확인하십시오."

#: templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "확인"

#: templates/account/email_confirm.html:27
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please <a href="
"\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr ""
"이 이메일 확인 링크가 만료되었거나 유효하지 않습니다.<a href=\"%(email_url)s"
"\"> 새로운 이메일 확인 요청을 발행</a>하십시오."

#: templates/account/login.html:7 templates/account/login.html:11
#: templates/account/login.html:59
msgid "Sign In"
msgstr "로그인"

#: templates/account/login.html:18
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign "
"up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"다음 중 하나로 로그인하십시오\n"
"기존 서드 파티 계정 또는 <a href=\"%(signup_url)s\">가입</a>\n"
"%(site_name)s 계정의 경우 아래에서 로그인하십시오:"

#: templates/account/login.html:22
#, python-format
msgid ""
"If you have a %(site_name)s\n"
"account that you haven't yet linked to any third party account, you must "
"log\n"
"in with your account's email address and password and once logged in, you "
"can\n"
"link your third party accounts via the Account Connections tab on your user\n"
"profile page."
msgstr ""

#: templates/account/login.html:28 templates/account/signup.html:17
#, python-format
msgid ""
"If you do not have a\n"
"%(site_name)s account but you have one of these third party accounts, you "
"can\n"
"create a %(site_name)s account and sign-in in one step by clicking the "
"third\n"
"party account."
msgstr ""

#: templates/account/login.html:41 templates/account/signup.html:30
#: templates/django_mailman3/profile/profile.html:72
msgid "or"
msgstr "또는"

#: templates/account/login.html:48
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"아직 계정을 만들지 않았다면\n"
"먼저 <a href=\"%(signup_url)s\">회원가입</a>하십시오."

#: templates/account/login.html:61
msgid "Forgot Password?"
msgstr "비밀번호를 잊으셨습니까?"

#: templates/account/logout.html:5 templates/account/logout.html:8
#: templates/account/logout.html:17
msgid "Sign Out"
msgstr "탈퇴하다"

#: templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "정말로 탈퇴하시겠습니까?"

#: templates/account/password_change.html:12
#: templates/account/password_reset_from_key.html:6
#: templates/account/password_reset_from_key.html:9
#: templates/django_mailman3/profile/base.html:20
msgid "Change Password"
msgstr "비밀번호 변경"

#: templates/account/password_reset.html:7
#: templates/account/password_reset.html:11
msgid "Password Reset"
msgstr "비밀번호 재설정"

#: templates/account/password_reset.html:16
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you "
"an e-mail allowing you to reset it."
msgstr ""
"비밀번호를 잊으셨습니까? 아래에 이메일 주소를 입력하면 재설정 할 수 있는 이메"
"일을 보내드립니다."

#: templates/account/password_reset.html:22
msgid "Reset My Password"
msgstr "내 비밀번호 재설정"

#: templates/account/password_reset.html:27
msgid "Please contact us if you have any trouble resetting your password."
msgstr "당신의 비밀번호를 재설정하는 데 문제가 발생하면 우리에게 연락주세요."

#: templates/account/password_reset_from_key.html:9
msgid "Bad Token"
msgstr "잘못된 토큰"

#: templates/account/password_reset_from_key.html:13
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</"
"a>."
msgstr ""
"비밀번호 재설정 링크는 이미 사용된 등의 이유로 유효하지 않습니다. <a href="
"\"%(passwd_reset_url)s\"> 새 비밀번호 재설정 </a>을 요청하십시오."

#: templates/account/password_reset_from_key.html:20
msgid "change password"
msgstr "비밀번호 변경"

#: templates/account/password_reset_from_key.html:25
msgid "Your password is now changed."
msgstr "당신의 비밀번호가 바뀌었습니다."

#: templates/account/password_set.html:12
msgid "Set Password"
msgstr "비밀번호 설정"

#: templates/account/signup.html:7 templates/socialaccount/signup.html:6
msgid "Signup"
msgstr "회원가입"

#: templates/account/signup.html:10 templates/account/signup.html:42
#: templates/socialaccount/signup.html:9 templates/socialaccount/signup.html:21
msgid "Sign Up"
msgstr "회원가입"

#: templates/account/signup.html:12
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr ""
"계정이 있으신가요? 그렇다면 <a href=\"%(login_url)s\">로그인</a>해 주세요."

#: templates/django_mailman3/paginator/pagination.html:45
msgid "Jump to page:"
msgstr "페이지로 이동:"

#: templates/django_mailman3/paginator/pagination.html:64
msgid "Results per page:"
msgstr "페이지 당 결과:"

#: templates/django_mailman3/paginator/pagination.html:80
#: templates/django_mailman3/profile/profile.html:71
msgid "Update"
msgstr "업데이트"

#: templates/django_mailman3/profile/base.html:6
msgid "User Profile"
msgstr "사용자 프로필"

#: templates/django_mailman3/profile/base.html:13
msgid "User profile"
msgstr "사용자 프로필"

#: templates/django_mailman3/profile/base.html:13
msgid "for"
msgstr "-"

#: templates/django_mailman3/profile/base.html:23
msgid "E-mail Addresses"
msgstr "이메일 주소"

#: templates/django_mailman3/profile/base.html:30
msgid "Account Connections"
msgstr "계정 연결"

#: templates/django_mailman3/profile/base.html:35
#: templates/django_mailman3/profile/delete_profile.html:16
msgid "Delete Account"
msgstr "계정 삭제"

#: templates/django_mailman3/profile/delete_profile.html:11
msgid ""
"Are you sure you want to delete your account? This will remove your account "
"along with all your subscriptions."
msgstr "계정을 삭제 하시겠습니까? 모든 가입목록과 함께 계정이 제거됩니다."

#: templates/django_mailman3/profile/profile.html:20
#: templates/django_mailman3/profile/profile.html:57
msgid "Edit on"
msgstr "편집하세요"

#: templates/django_mailman3/profile/profile.html:28
msgid "Primary email:"
msgstr "이전 이메일:"

#: templates/django_mailman3/profile/profile.html:34
msgid "Other emails:"
msgstr "다른 이메일:"

#: templates/django_mailman3/profile/profile.html:40
msgid "(no other email)"
msgstr "(다른 이메일 없음)"

#: templates/django_mailman3/profile/profile.html:45
msgid "Link another address"
msgstr "다른 주소 링크하기"

#: templates/django_mailman3/profile/profile.html:53
msgid "Avatar:"
msgstr "아바타:"

#: templates/django_mailman3/profile/profile.html:63
msgid "Joined on:"
msgstr "가입됨:"

#: templates/django_mailman3/profile/profile.html:72
msgid "cancel"
msgstr "취소"

#: templates/openid/login.html:10
msgid "OpenID Sign In"
msgstr "OpenID 로그인"

#: templates/socialaccount/connections.html:9
msgid ""
"You can sign in to your account using any of the following third party "
"accounts:"
msgstr "다음 서드 파티 계정을 사용하여 계정에 로그인할 수 있습니다:"

#: templates/socialaccount/connections.html:42
msgid ""
"You currently have no social network accounts connected to this account."
msgstr "현재 이 계정에 연결된 소셜 네트워크 계정이 없습니다."

#: templates/socialaccount/connections.html:45
msgid "Add a 3rd Party Account"
msgstr "서드 파티 계정 추가"

#: templates/socialaccount/signup.html:11
#, python-format
msgid ""
"You are about to use your %(provider_name)s account to login to\n"
"%(site_name)s. As a final step, please complete the following form:"
msgstr ""
"귀하의 %(provider_name)s 계정을 이용하여\n"
"%(site_name)s에 로그인하려 합니다. 다음 양식을 완성하십시오:"

#: templatetags/pagination.py:43
msgid "Newer"
msgstr "새로운"

#: templatetags/pagination.py:44
msgid "Older"
msgstr "오래된"

#: templatetags/pagination.py:46
msgid "Previous"
msgstr "이전"

#: templatetags/pagination.py:47
msgid "Next"
msgstr "다음"

#: views/profile.py:72
msgid "The profile was successfully updated."
msgstr "프로필이 성공적으로 업데이트되었습니다."

#: views/profile.py:74
msgid "No change detected."
msgstr "변경 사항이 감지되지 않았습니다."

#: views/profile.py:110
msgid "Successfully deleted account"
msgstr "성공적으로 삭제된 계정"
